package org.dt.bexon.phonestateinfo

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.telephony.SubscriptionManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // If app don't get permission, request it
            val permission: Array<String> = arrayOf(Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE)
            ActivityCompat.requestPermissions(this, permission, REQUESTCODE)
        } else {
            sim()
        }
    }

    private fun sim() {
        val textView = findViewById<TextView>(R.id.textInfo)
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                val iccIdList = mutableListOf<String>() // ICC ID List
                val phoneNumList = mutableListOf<String>() // Phone Number List
                val carrierNameList = mutableListOf<String>() // carrier List
                val countryIsoList = mutableListOf<String>() // ISO List

                val sm = getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE) as SubscriptionManager
                val sis = sm.activeSubscriptionInfoList
                if (sis.size >= 1) {
                    val si1 = sis[0]
                    val iccId1 = si1.iccId
                    val carrier1 = si1.carrierName
                    val phoneNum1 = si1.number
                    val countryIso1 = si1.countryIso
                    iccIdList.add(0, iccId1)
                    phoneNumList.add(0, phoneNum1)
                    carrierNameList.add(0, carrier1 as String)
                    countryIsoList.add(0, countryIso1)
                }
                // If dual cards are supported
                if (sis.size >= 2) {
                    val si2 = sis[1]
                    val iccId2 = si2.iccId
                    val carrier2 = si2.carrierName
                    val phoneNum2 = si2.number
                    val countryIso2 = si2.countryIso
                    iccIdList.add(1, iccId2)
                    phoneNumList.add(1, phoneNum2)
                    carrierNameList.add(1, carrier2 as String)
                    countryIsoList.add(1, countryIso2)
                }
                // get information about the number of SIM cards:
                val count = sm.activeSubscriptionInfoCount //The current actual number of cards
                val max = sm.activeSubscriptionInfoCountMax //The number of current card slots
                var info = ""
                if (count > 0) {
                    info += """
                    SIM card slots: $max
                    SIM card slot(used): $count
                    
                    ICCID-1: ${iccIdList[0]}
                    Phone number-1: ${phoneNumList[0]}
                    Telecom operator-1: ${carrierNameList[0]}
                    Country Iso-1: ${countryIsoList[0]}
                """.trimIndent()
                } else {
                    info = "No SIM"
                }

                if (count > 1) {
                    info += """
                        
                        
                        ICCID-2: ${iccIdList[1]}
                        Phone number-2: ${phoneNumList[1]}
                        Telecom operator-2: ${carrierNameList[1]}
                        Country Iso-2: ${countryIsoList[1]}
                    """.trimIndent()
                }
                textView.text = info
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUESTCODE -> {
                sim()
            }
        }
    }

    companion object {
        val REQUESTCODE = 6001
    }
}