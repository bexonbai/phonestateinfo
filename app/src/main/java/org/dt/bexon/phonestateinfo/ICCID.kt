package org.dt.bexon.phonestateinfo

import android.content.Context
import java.io.InputStream
import java.util.*

object ICCID {
    fun getName(icc: String, context: Context): String {
        var name = ""
        val pro = Properties()
        val inputStream: InputStream = context.assets.open("icc.properties")
        pro.load(inputStream)

        for (key in pro.stringPropertyNames()) {
            if (icc.startsWith(key)) {
                name = pro.getProperty(key, "")
            }
        }
        return name
    }
}